// Selector 
const todoInput = document.querySelector('.todo-input'); 
const todoButton = document.querySelector('.todo-button'); 
const todoList = document.querySelector('.todo-list'); 
const fileterOption = document.querySelector('.filter-todo');


// Event Listeners
document.addEventListener('DOMContentLoaded', getTodos);
todoButton.addEventListener('click', addTodo);
todoList.addEventListener('click', deleteChecked);
fileterOption.addEventListener('click', filterTodo);



// Funcitons
function addTodo(event){
    //prevent from submitting
    event.preventDefault();

    //Create todo div
    const todoDiv = document.createElement('div');
    todoDiv.classList.add('todo');

    //Create li 
    const todoItemLi = document.createElement('li');;
    todoItemLi.innerText = todoInput.value;
    todoItemLi.classList.add('todo-item');
    //add todoItem li to the parent div
    todoDiv.appendChild(todoItemLi)

    // ADD TODO TO LOCALSTORAGE
    saveLocalTodos(todoInput.value);

    //Create Checked Button
    const completedButton = document.createElement('button');
    completedButton.innerHTML = '<i class="fas fa-check"></i>';
    completedButton.classList.add("complete-btn");
    //add checked button to div
    todoDiv.appendChild(completedButton);

    const trashButton = document.createElement('button');
    trashButton.innerHTML = '<i class="fas fa-trash"></i>';
    trashButton.classList.add("trash-btn");
    //add checked button to div
    todoDiv.appendChild(trashButton);

    //Append to list 
    todoList.appendChild(todoDiv);

    //clear todo input value
    todoInput.value = '';
}

function deleteChecked(e){
    
    const item = e.target;
    //DELETE TODO
    if(item.classList[0]==="trash-btn"){
        const todo = item.parentElement;
        
        //animation
        todo.classList.add("fall");
        removeLocalTodos(todo);
        todo.addEventListener('transitionend', function(){
            todo.remove();
        });
        
        
    }
    //CHECK MARK
    if(item.classList[0]==="complete-btn"){
        const todo = item.parentElement;
        todo.classList.toggle("completed")
    }
};

function filterTodo(event){    
    const todos = todoList.childNodes;
    // console.log(todos);
    todos.forEach(function(todo) {
        // console.log(todo.childNodes);
        switch(event.target.value){
            case "all":
                todo.style.display='flex';
                break;
            case "completed":
                if(todo.classList.contains('completed')){
                    todo.style.display='flex';
                }else{
                    todo.style.display = "none";
                }
                break;
            case "uncompleted":
                if(!todo.classList.contains('completed')){
                    todo.style.display='flex';
                }else{
                    todo.style.display = "none";
                }
                break;
                
        }
    });
}

function saveLocalTodos(todo){
    //CHECK ---- Does the todo's already stored into database 
    let todos;
    if(localStorage.getItem('todos') === null){
        todos = [];
    }else{
        todos = JSON.parse(localStorage.getItem('todos'));
        console.log(todos);
    }
    todos.push(todo);
    localStorage.setItem('todos', JSON.stringify(todos));
}

function getTodos(){
    // console.log("hello");
    //CHECK -- DO I already have thin in there
    let todos;
    if(localStorage.getItem("todos") === null){
        todos = [];
    } else{
        todos = JSON.parse(localStorage.getItem("todos"));
    }


    todos.forEach(function(todo){
    //COPIED FROM OTHER FUNCITON
    const todoDiv = document.createElement('div');
    todoDiv.classList.add('todo');

    //Create li 
    const newTodo = document.createElement('li');;
    newTodo.innerText = todo;
    newTodo.classList.add('todo-item');
    //add todoItem li to the parent div
    todoDiv.appendChild(newTodo)

    //Create Checked Button
    const completedButton = document.createElement('button');
    completedButton.innerHTML = '<i class="fas fa-check"></i>';
    completedButton.classList.add("complete-btn");
    //add checked button to div
    todoDiv.appendChild(completedButton);

    const trashButton = document.createElement('button');
    trashButton.innerHTML = '<i class="fas fa-trash"></i>';
    trashButton.classList.add("trash-btn");
    //add checked button to div
    todoDiv.appendChild(trashButton);

    //Append to list 
    todoList.appendChild(todoDiv);

    });
}


function removeLocalTodos(todo){
    //CHECK -- DO I already have thin in there
    let todos;
    if(localStorage.getItem("todos")===null){
        todos = [];
    } else{
        todos = JSON.parse(localStorage.getItem("todos"));
    }
    const todoIndex = todo.children[0].innerText;    
    todos.splice(todos.indexOf(todoIndex), 1);
    // console.log(todos);
    localStorage.setItem("todos", JSON.stringify(todos));    
}

// const todos = ['apple', 'john', 'dount', 'babyboy' ];
// const johnIndex = todos.indexOf('john');
// console.log(johnIndex);
// todos.splice(johnIndex, 1);
// console.log(todos);
// console.log(todos.indexOf('dount'));


